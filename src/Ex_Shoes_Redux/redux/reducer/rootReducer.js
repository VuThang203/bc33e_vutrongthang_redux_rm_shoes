import { combineReducers } from "redux";
import { shoeReducer } from "./shoesReducer";

export const rootReducer_Ex_Shoes_Redux = combineReducers({ shoeReducer });
