import React, { Component } from "react";
import { data_shoes } from "./data_shoes";
import GioHang from "./GioHang";
import ItemShoe from "./ItemShoe";
import { shoeReducer } from "./redux/reducer/shoesReducer";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constants/shoesConstants";

class Ex_Shoes_Redux extends Component {
  state = {
    shoes: data_shoes,
    gioHang: [],
  };

  renderContent = () => {
    return this.props.listShoes.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          handleAddToCart={this.props.handleAddToCartRedux}
          data={item}
        />
      );
    });
  };

  handleAddToCart = (shoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == shoe.id;
    });

    let cloneGioHang = [...this.state.gioHang];
    // th1 trong giỏi hàng chưa có sản phẩm

    if (index == -1) {
      let newSp = { ...shoe, soLuong: 1 };
      cloneGioHang.push(newSp);
    } else {
      // th2 trong giỏi hàng đã có sản phẩm

      cloneGioHang[index].soLuong++;
    }

    this.setState({ gioHang: cloneGioHang });
  };
  // 2 + -1
  handleChangeQuantity = (idShoe, value) => {
    let index = this.state.gioHang.findIndex((shoe) => {
      return shoe.id == idShoe;
    });

    if (index == -1) return;

    let cloneGioHang = [...this.state.gioHang];

    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + value;

    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);

    this.setState({ gioHang: cloneGioHang });
  };
  render() {
    console.log("in render", this.state.gioHang.length);

    return (
      <div className="container py-5">
        <GioHang
          handleChangeQuantity={this.handleChangeQuantity}
          gioHang={this.state.gioHang}
        />
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listShoes: state.shoeReducer.shoes,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCartRedux: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Ex_Shoes_Redux);
